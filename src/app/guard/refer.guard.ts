import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../auth/model/user';

@Injectable({
  providedIn: 'root'
})
export class ReferGuard implements CanActivate {

  private static loggedIn: string = 'loggegInUser';

  constructor(private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (localStorage.getItem(ReferGuard.loggedIn)) {
      let user: User = JSON.parse(localStorage.getItem(ReferGuard.loggedIn) as string);

      if (user) {
        this.router.navigateByUrl('/event');
      }


    }

    return true;
  }

}
