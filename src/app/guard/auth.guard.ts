import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../auth/model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  private static loggedIn: string = 'loggegInUser';
  constructor(private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (localStorage.getItem(AuthGuard.loggedIn)) {
      let user: User = JSON.parse(localStorage.getItem(AuthGuard.loggedIn) as string);

      if (!user) {
        this.router.navigateByUrl('/');
      }


    } else {
      this.router.navigateByUrl('/');
    }

    return true;

  }

}
