import { Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventService } from 'src/app/services/event.service';
import { Events } from '../model/event';
import { ModalComponent } from '../shared/modal/modal.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  events!: Events[];
  loading: boolean = true;

  constructor(private _modalService: NgbModal, private eventService: EventService, private router: Router) { }

  async ngOnInit() {
    try {
      this.events = await this.eventService.getEvent();
      if (this.events) {
        this.loading = false;
      }
    } catch (err) {
      if (err == 'No Value') {
        this.loading = false;
        this.events = [];
      }
    }
  }

  updateEvent(id: string) {
    if (id) {
      this.router.navigateByUrl(`/event/edit/${id}`);
    }
  }

  async open(id: string) {
    try {
      let res: NgbModalRef = this._modalService.open(ModalComponent);
      if (await res.result === true) {
        let deleted = this.eventService.deleteEvent(id);
        if (deleted.status) {
          this.ngOnInit();
        }
      }
    } catch (err) {

    }
  }



}
