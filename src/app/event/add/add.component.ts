import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Events } from '../model/event';
//@ts-ignore 
import { v4 as uuidv4 } from 'uuid';
import { EventService } from 'src/app/services/event.service';
import { ResponseObj } from 'src/app/services/auth.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  eventFormGroup!: FormGroup;
  error!: string;
  progressBarwidth: number = 0;
  showProgress: boolean = false;

  showToast = false;
  autohideToast = true;

  constructor(private router: Router, private fb: FormBuilder, private eventService: EventService) {
    this.eventFormGroup = this.fb.group(
      {
        name: new FormControl('', [Validators.required]),
        description: new FormControl('', [Validators.required]),
        date: new FormControl('', [Validators.required])
      }
    );
  }

  ngOnInit(): void {
  }

  get f() { return this.eventFormGroup.controls; }

  save() {
    let obj: Events = {
      name: this.f.name.value.toString().trim(),
      description: this.f.description.value.toString().trim(),
      date: this.f.date.value.toString().trim(),
      id: uuidv4()
    }

    let res: ResponseObj = this.eventService.addNewEvent(obj);
    this.showProgress = true;
    let timer = setInterval(() => { this.progressBarwidth += 1 }, 1);
    if (this.progressBarwidth === 100) {
      clearInterval(timer);
    }

    setTimeout(() => {
      if (!res.status) {
        this.error = res.message;

        setTimeout(() => {
          this.error = '';
          this.showProgress = false;
        }, 3000);
      } else {
        this.showToast = true;
        this.router.navigateByUrl('/event');
      }
    }, 2500);

  }

}
