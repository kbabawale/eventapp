import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Events } from '../model/event';
import { EventService } from 'src/app/services/event.service';
import { ResponseObj } from 'src/app/services/auth.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  eventFormGroup!: FormGroup;
  error!: string;
  progressBarwidth: number = 0;
  showProgress: boolean = false;

  event!: Events;
  eventParam!: string;

  constructor(private router: Router, private fb: FormBuilder, private eventService: EventService, private route: ActivatedRoute) {
    this.eventFormGroup = this.fb.group(
      {
        name: new FormControl('', [Validators.required]),
        description: new FormControl('', [Validators.required]),
        date: new FormControl('', [Validators.required])
      }
    );
  }

  async ngOnInit() {
    try {
      this.route.params.subscribe(params => {
        this.eventParam = params.id;
      });
      let found = await this.eventService.getEvent(this.eventParam);
      this.event = found[0];
      if (this.event) {

        this.patchValues(this.event);
      }
    } catch (err) {
      if (err == 'No Value') {
        this.router.navigateByUrl('/event');
      }
    }
  }

  patchValues(event: Events) {
    this.eventFormGroup.patchValue({
      name: event.name,
      description: event.description,
      date: event.date
    });
  }

  get f() { return this.eventFormGroup.controls; }

  save() {
    let obj: Events = {
      name: this.f.name.value.toString().trim(),
      description: this.f.description.value.toString().trim(),
      date: this.f.date.value.toString().trim(),
      id: this.event.id
    }

    let res: ResponseObj = this.eventService.updateEvent(obj);
    this.showProgress = true;
    let timer = setInterval(() => { this.progressBarwidth += 1 }, 1);
    if (this.progressBarwidth === 100) {
      clearInterval(timer);
    }

    setTimeout(() => {
      if (!res.status) {
        this.error = res.message;

        setTimeout(() => {
          this.error = '';
          this.showProgress = false;
        }, 2500);
      } else {
        this.router.navigateByUrl('/event');
      }
    }, 2500);

  }

}
