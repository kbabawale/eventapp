import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/auth/model/user';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private static loggegInUser: string = 'loggegInUser';
  loggedInUserName!: string;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    if (localStorage.getItem(HeaderComponent.loggegInUser)) {
      let userResponse: User = JSON.parse(localStorage.getItem(HeaderComponent.loggegInUser) as string);
      this.loggedInUserName = userResponse.firstname;
    }

  }

  logout() {
    this.authService.logout();
  }

}
