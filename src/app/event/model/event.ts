export interface Events {
    id: string;
    name: string;
    date: Date;
    description: string;
}