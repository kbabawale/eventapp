import { Injectable } from '@angular/core';
import { User } from '../auth/model/user';
//@ts-ignore
import validator from 'validator';
//@ts-ignore
import * as CryptoJS from 'crypto-js';
import { Router } from '@angular/router';

export interface ResponseObj {
  status: boolean;
  message: string
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private static UserDetail: string = 'userDetail';
  private static loggegInUser: string = 'loggegInUser';
  secretKey = "YourSecretKeyForEncryption&Descryption";

  constructor(private router: Router) { }

  login(user: { username: string, password: string }): ResponseObj {
    let res: ResponseObj = { status: false, message: '' };
    this.initStore();
    if (localStorage.getItem(AuthService.UserDetail)) {
      let storeResponse: User[] = JSON.parse(localStorage.getItem(AuthService.UserDetail) as string);

      let found = storeResponse.filter(x => x.username === user.username && this.decrypt(x.password) === user.password);
      if (found.length > 0) {
        localStorage.setItem(AuthService.loggegInUser, JSON.stringify(found[0]));
        res = { message: 'Login successful', status: true };
      } else {
        res = { message: 'Login failed', status: false };
      }
    } else {
      res = { status: false, message: 'Login failed' };
    }
    return res;
  }

  register(user: User): ResponseObj {
    let res: ResponseObj = { status: false, message: '' };
    this.initStore();

    //validate inputs
    if (!validator.isAlpha(user.firstname) || !validator.isAlpha(user.lastname) || !validator.isAlpha(user.username)) {
      res = { message: 'Provide valid names', status: false };
    }
    if (!validator.isEmail(user.email)) {
      res = { message: 'Provide valid email address', status: false };
    }
    if (validator.isEmpty(user.email) || validator.isEmpty(user.firstname) || validator.isEmpty(user.lastname) || validator.isEmpty(user.password) || validator.isEmpty(user.username)) {
      res = { message: 'All fields are required', status: false };
    }

    user.username = validator.escape(user.username);
    user.email = validator.escape(user.email);
    user.firstname = validator.escape(user.firstname);
    user.lastname = validator.escape(user.lastname);

    //if values exists
    if (localStorage.getItem(AuthService.UserDetail)) {
      let storeResponse: User[] = JSON.parse(localStorage.getItem(AuthService.UserDetail) as string);

      //prevent duplicates
      if (storeResponse.findIndex(x => x.email === user.email) > -1) {
        res = { message: 'Email already exists', status: false };
      } else {
        let newStoreResponse = [...storeResponse, user];

        localStorage.setItem(AuthService.UserDetail, JSON.stringify(newStoreResponse));

        //create session
        localStorage.setItem(AuthService.loggegInUser, JSON.stringify(user));
      }



    } else { //add new value to store
      localStorage.setItem(AuthService.UserDetail, JSON.stringify([user]));

      //create session
      localStorage.setItem(AuthService.loggegInUser, JSON.stringify(user));
      res = { status: true, message: 'Ok' };
    }
    return res;


  }

  logout() {
    localStorage.removeItem(AuthService.loggegInUser);
    this.router.navigateByUrl('/');
  }

  initStore() {
    if (!localStorage.getItem(AuthService.UserDetail)) {
      localStorage.setItem(AuthService.UserDetail, '');
    }
  }

  decrypt(textToDecrypt: string) {
    return CryptoJS.AES.decrypt(textToDecrypt, this.secretKey.trim()).toString(CryptoJS.enc.Utf8);
  }
}
