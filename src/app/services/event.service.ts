import { Injectable } from '@angular/core';
//@ts-ignore
import validator from 'validator';
import { ResponseObj } from './auth.service';
import { Events } from '../event/model/event';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  private static EventsDetail: string = 'eventsDetail';

  constructor() { }

  addNewEvent(event: Events): ResponseObj {
    let res: ResponseObj = { status: false, message: '' };
    this.initStore();

    //validate inputs
    if (validator.isEmpty(event.date) || validator.isEmpty(event.name) || validator.isEmpty(event.description)) {
      res = { message: 'All fields are required', status: false };
    }
    if (!validator.isDate(event.date)) {
      res = { message: 'Provide valid date', status: false };
    }

    event.name = validator.escape(event.name);
    event.description = validator.escape(event.description);
    event.date = validator.escape(event.date);

    //if values exists
    if (localStorage.getItem(EventService.EventsDetail)) {
      let storeResponse: Events[] = JSON.parse(localStorage.getItem(EventService.EventsDetail) as string);

      //prevent duplicates
      if (storeResponse.findIndex(x => x.name === event.name) > -1) {
        res = { message: 'Event already exists', status: false };
      } else {
        let newStoreResponse = [...storeResponse, event];

        localStorage.setItem(EventService.EventsDetail, JSON.stringify(newStoreResponse));
        res = { status: true, message: 'Ok' };
      }



    } else { //add new value to store
      localStorage.setItem(EventService.EventsDetail, JSON.stringify([event]));

      res = { status: true, message: 'Ok' };
    }
    return res;


  }

  getEvent(id: string = 'all'): Promise<Events[]> {
    this.initStore();
    return new Promise((resolve, reject) => {
      if (id !== 'all') { //fetch one event
        if (!localStorage.getItem(EventService.EventsDetail)) {
          reject('No Value');
        }
        let storeResponse: Events[] = JSON.parse(localStorage.getItem(EventService.EventsDetail) as string);
        let found = storeResponse.filter(x => x.id === id)[0];
        if (!found) {
          reject('No Value');
        }
        resolve([found]);
      } else {
        if (!localStorage.getItem(EventService.EventsDetail)) {
          reject('No Value');
        }
        let storeResponse: Events[] = JSON.parse(localStorage.getItem(EventService.EventsDetail) as string);

        if (!storeResponse) {
          reject('No Value');
        }
        resolve(storeResponse);
      }
    });


  }

  updateEvent(event: Events): ResponseObj {
    let res: ResponseObj = { status: false, message: '' };

    //validate inputs
    if (validator.isEmpty(event.date) || validator.isEmpty(event.name) || validator.isEmpty(event.description)) {
      res = { message: 'All fields are required', status: false };
    }
    if (!validator.isDate(event.date)) {
      res = { message: 'Provide valid date', status: false };
    }

    event.name = validator.escape(event.name);
    event.description = validator.escape(event.description);
    event.date = validator.escape(event.date);

    //if values exists
    if (localStorage.getItem(EventService.EventsDetail)) {
      let storeResponse: Events[] = JSON.parse(localStorage.getItem(EventService.EventsDetail) as string);

      //prevent duplicates
      let found: number = storeResponse.findIndex(x => x.id === event.id);
      if (found > -1) {
        //remove item
        storeResponse.splice(found, 1);
        //add new
        let newStore = [...storeResponse, event];
        localStorage.setItem(EventService.EventsDetail, JSON.stringify(newStore));
        res = { status: true, message: 'Ok' };
      } else {
        res = { message: 'An Error Occurred', status: false };
      }
    } else {
      res = { message: 'An Error Occurred', status: false };
    }

    return res;
  }

  deleteEvent(id: string): ResponseObj {
    let res: ResponseObj = { status: false, message: '' };

    //if values exists
    if (localStorage.getItem(EventService.EventsDetail)) {
      let storeResponse: Events[] = JSON.parse(localStorage.getItem(EventService.EventsDetail) as string);

      //prevent duplicates
      let found: number = storeResponse.findIndex(x => x.id === id);
      if (found > -1) {
        //remove item
        storeResponse.splice(found, 1);

        localStorage.setItem(EventService.EventsDetail, JSON.stringify(storeResponse));
        res = { status: true, message: 'Ok' };
      } else {
        res = { message: 'An Error Occurred', status: false };
      }

    } else {
      res = { message: 'An Error Occurred', status: false };
    }

    return res;
  }

  initStore() {
    if (!localStorage.getItem(EventService.EventsDetail)) {
      localStorage.setItem(EventService.EventsDetail, '');
    }
  }
}
