import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';
import { ReferGuard } from './guard/refer.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [ReferGuard],
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'event',
    canActivate: [AuthGuard],
    loadChildren: () => import('./event/event.module').then(m => m.EventModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
