import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../model/user';

//@ts-ignore 
import { v4 as uuidv4 } from 'uuid';
//@ts-ignore
import * as CryptoJS from 'crypto-js';
import { AuthService, ResponseObj } from 'src/app/services/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  activeId: number = 1;
  registerFormGroup!: FormGroup;
  loginFormGroup!: FormGroup;
  secretKey = "YourSecretKeyForEncryption&Descryption";
  error!: string;
  progressBarwidth: number = 0;
  showProgress: boolean = false;

  showToast = false;
  autohideToast = true;

  links = [
    { title: 'Login', fragment: 1 },
    { title: 'Register', fragment: 2 }
  ];

  constructor(public route: ActivatedRoute, private router: Router, private fb: FormBuilder, private authService: AuthService) {
    this.registerFormGroup = this.fb.group(
      {
        firstname: new FormControl('', [Validators.required]),
        lastname: new FormControl('', [Validators.required]),
        username: new FormControl('', [Validators.required]),
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', [Validators.required, Validators.minLength(8)])
      }
    );
    this.loginFormGroup = this.fb.group(
      {
        username: new FormControl('', [Validators.required]),
        password: new FormControl('', [Validators.required])
      }
    );
  }

  get f() { return this.registerFormGroup.controls; }
  get g() { return this.loginFormGroup.controls; }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.activeId = data.state;
    })
  }

  setPage(destination: number) {
    this.activeId = destination;
  }

  encrypt(value: string): string {
    return CryptoJS.AES.encrypt(value, this.secretKey.trim()).toString();
  }

  login() {
    let obj: { username: string, password: string } = {
      username: this.g.username.value.toString().trim(),
      password: this.g.password.value.toString().trim()
    }

    let res: ResponseObj = this.authService.login(obj);
    this.showProgress = true;
    let timer = setInterval(() => { this.progressBarwidth += 1 }, 2);
    if (this.progressBarwidth === 100) {
      clearInterval(timer);
    }

    setTimeout(() => {
      if (!res.status) {
        this.error = res.message;

        setTimeout(() => {
          this.error = '';
          this.showProgress = false;
        }, 3000);
      } else {
        this.showToast = true;
        this.router.navigateByUrl('/event');
      }
    }, 2500);
  }

  register() {

    const passwordHash = this.encrypt(this.f.password.value.toString().trim());

    let obj: User = {
      firstname: this.f.firstname.value.toString().trim(),
      lastname: this.f.lastname.value.toString().trim(),
      username: this.f.username.value.toString().trim(),
      email: this.f.email.value.toString().trim(),
      password: passwordHash,
      id: uuidv4()
    }

    let res: ResponseObj = this.authService.register(obj);
    this.showProgress = true;
    let timer = setInterval(() => { this.progressBarwidth += 1 }, 5);
    if (this.progressBarwidth === 100) {
      clearInterval(timer);
    }

    setTimeout(() => {
      if (!res.status) {
        this.error = res.message;

        setTimeout(() => {
          this.error = '';
          this.showProgress = false;
        }, 3000);
      } else {
        this.showToast = true;
        this.router.navigateByUrl('/event');
      }
    }, 3000);

  }

  goToNext() {
    this.router.navigateByUrl('/event');
  }


}
